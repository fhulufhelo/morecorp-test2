<?php

namespace App\Controller;

use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Trikoder\Bundle\OAuth2Bundle\Manager\ClientManagerInterface;
use Trikoder\Bundle\OAuth2Bundle\Model\Client;
use Trikoder\Bundle\OAuth2Bundle\Model\Grant;
use Trikoder\Bundle\OAuth2Bundle\Model\RedirectUri;
use Trikoder\Bundle\OAuth2Bundle\Model\Scope;

class ProfileController extends AbstractController
{

    /**
     * @var ClientManagerInterface
     */
    private $clientManager;

    /**
     * ProfileController constructor.
     * @param ClientManagerInterface $clientManager
     * @param SessionInterface $session
     */
    public function __construct(ClientManagerInterface $clientManager)
    {
        $this->clientManager = $clientManager;
    }
    /**
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $client = $this->clientManager->find($this->getUser()->getUsername());

        return $this->render('profile/index.html.twig', [
            'user' => $this->getUser(),
            'client'=> $client
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function creatToken(Request $request)
    {
        $client = $this->buildClientFromInput($request);
        $this->clientManager->save($client);

        $this->addFlash('notice', 'New oAuth2 client created successfully.');

        return $this->redirectToRoute('profile');

    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function revoke(Request $request)
    {
        $client = $this->clientManager->find($this->getUser()->getUsername());
        $this->clientManager->remove($client);

        $this->addFlash('notice', 'Given oAuth2 client deleted successfully.');

        return $this->redirectToRoute('profile');

    }

    private function buildClientFromInput($input)
    {
        $identifier =  $this->getUser()->getUsername();
        $secret =  hash('sha512', random_bytes(32));

        $client = new Client($identifier, $secret);
        $client->setActive(true);
        $client->setAllowPlainTextPkce($input->get('allow-plain-text-pkce', false));

        $redirectUris = array_map(
            static function (string $redirectUri): RedirectUri { return new RedirectUri($redirectUri); },
            explode(', ', $input->get('redirect_uri', ''))
        );

        $client->setRedirectUris(...$redirectUris);

        $grants = array_map(
            static function (string $grant): Grant { return new Grant($grant); },
            ['authorization_code']
        );
        $client->setGrants(...$grants);

        $scopes = array_map(
            static function (string $scope): Scope { return new Scope($scope); },
            explode(', ', $input->get('scope', ''))
        );
        $client->setScopes(...$scopes);

        return $client;

    }

}
