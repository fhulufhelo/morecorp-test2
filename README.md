## instru
### Server

git clone `https://gitlab.com/fhulufhelo/morecorp-test2.git`

cd `morecorp-test2`

install  ` composer install`

 start server ` symfony server:start`
 
 login `http://127.0.0.1:8000/login`
 
  after login you will be redirected to `http://127.0.0.1:8000/profile`

create a client on `http://127.0.0.1:8000/profile`
![enter image description here](https://drive.google.com/uc?id=1zEc9jq_P-u_SYlnjydFIXjt7qGlHfdqU)

### Client
Request a token


        $request->session()->put('state',  $state =  Str::random(40));
    $query =  http_build_query([
    'client_id'  =>  'mokhomifhulufhelo@gmail.com',
    'redirect_uri'  =>  'http://127.0.0.1:3000/callback',
    'response_type'  =>  'code',
    'scope'  =>  '',
    'state'  =>  $state,
    ]);
    return  redirect('http://127.0.0.1:8000/authorize?'.$query);



wait on the callback

``` 
Route::get('/callback',  function  (Request  $request)  {
$state =  $request->session()->pull('state');
return  $request;
throw_unless(
strlen($state)  >  0  &&  $state ===  $request->state,
InvalidArgumentException::class
);
$http =  new GuzzleHttp\Client;
$response =  $http->post('http://127.0.0.1:8000/token',  [
'form_params'  =>  [
'grant_type'  =>  'authorization_code',
'client_id'  =>  'mokhomifhulufhelo@gmail.com',
'client_secret'  =>  'd49fa4fe0854f5a8d6a651615ba6405261ba7db222b3e82f403d5b6e4755473fab9555940ef67cd4cbba4ad18a8ff8db82b962f2a61ffecbbb3c5928f7dbe487
',
'redirect_uri'  =>  'http://127.0.0.1:9000/callback',
'code'  =>  $request->code,
],
]);
return  json_decode((string)  $response->getBody(),  true);
});
```

